import printUser from './index-good'

describe('print', () => {
  let user
  global.console = {
    log: jest.fn()
  }

  beforeEach(() => {
    user = {
      firstname: 'Albert',
      lastname: 'Dupont',
      role: 'admin',
      isManager: true
    }
  })

  it('should print super administrateur', () => {
    printUser(user)

    expect(global.console.log).toBeCalledWith('Albert est super administrateur')
  })

  it('should print administrateur', () => {
    user.isManager = false
    printUser(user)

    expect(global.console.log).toBeCalledWith('Albert est administrateur')
  })

  it('should print utilisateur', () => {
    user.role = 'user'
    printUser(user)

    expect(global.console.log).toBeCalledWith('Albert est utilisateur')
  })
})