const userRoles = [
  { label: 'est super administrateur', condition: isSuperAdmin },
  { label: 'est administrateur', condition: isAdmin },
  { label: 'est utilisateur', condition: isEmployee }
]

export default function printUser({ role, isManager, firstname }) {
  const roleLabel = getUserRole({ role, isManager })
  window.console.log(`${firstname} ${roleLabel}`)
}

function getUserRole({ role, isManager }) {
  return userRoles.reduce((label, userRole) => {
    label += userRole.condition({ role, isManager }) ? userRole.label : ''
    return label
  }, '')
}

function isSuperAdmin({ role, isManager }) {
  return hasAdminRole({ role }) && isManager
}

function isAdmin({ role, isManager }) {
  return hasAdminRole({ role }) && !isManager
}

function isEmployee({ role }) {
  return !hasAdminRole({ role })
}

function hasAdminRole({ role }) {
  return role === 'admin'
}
