

export default function print(user) {
  if (user.role === 'admin' && user.isManager) {
    window.console.log(`${user.firstname} est super administrateur`)
  } else {
    if (user.role === 'admin') {
      window.console.log(`${user.firstname} est administrateur`)
    } else {
      window.console.log(`${user.firstname} est utilisateur`)
    }
  }
}

/*
  const user = {
    firstname: 'Albert',
    lastname: 'Dupont',
    role: 'admin',
    isManager: true,
    address: '1 rue des écoles 75005 Paris'
  }
  print(user)
*/
